package com.dkit.gd2;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Example {
    public static void main(String[] args) {
        try {
            int result = divide();
            System.out.println(result);
        }catch(ArithmeticException | NoSuchElementException e) {
            System.out.println(e.toString());
            System.out.println("Unable to perform division, shutting down");
        }

    }

    private static int divide(){
        int x;
        int y;
        x = getInt();
        y = getInt();
        return x/y;
    }

    private static int getInt(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter an integer");
        while(true) {
            try {
                return sc.nextInt();
            }catch(InputMismatchException e){
                //Go around again. Trash input first
                sc.nextLine();
                System.out.println("Please enter a number using only the digits 0 to 9");
            }
        }
    }

}
